FROM ruby:2.6.1-alpine

RUN apk update && apk add build-base postgresql-dev tzdata

ENV DOCKERIZE_VERSION v0.6.1
RUN wget https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-alpine-linux-amd64-$DOCKERIZE_VERSION.tar.gz

RUN mkdir /app
WORKDIR /app

RUN gem install bundler

COPY Gemfile Gemfile.lock ./

RUN bundle check || bundle install --binstubs "$BUNDLE_BIN" --jobs 10 --retry 5

COPY . .

LABEL maintainer="Hebert Douglas <hebertdougl@gmail.com>"

CMD dockerize -wait tcp://postgresql:5432 -timeout 300s -wait-retry-interval 30s bundle exec puma -v -C config/puma.rb