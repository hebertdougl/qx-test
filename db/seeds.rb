# frozen_string_literal: true

User.create(username: ENV['DEFAULT_USERNAME'], password: ENV['DEFAULT_PASSWORD'])
