# frozen_string_literal: true

class AddIssueIdToEvents < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :issue_id, :integer
  end
end
