# frozen_string_literal: true

Rails.application.routes.draw do
  post '/events', to: 'events#create'
  get '/issues/:issue_id/actions', to: 'actions#show', as: 'action'
end
