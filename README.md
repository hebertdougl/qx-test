# Desafio QCX

Essa aplicação consiste em uma aplicação que escuta os eventos (nessa caso armazenaremos as issues) do Github via webhooks e fornece um endpoint para visualização de todoas as ações vinculadas a issue.

## Ambiente

Para fazer o clone e executar o projeto é necessário ter instalado na maquina as seguintes ferramentas.

- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Ngrok](https://ngrok.com/download)

Toda a aplicação roda utilizando a containers docker e para facilitar a criação de multiplos containers estamos utilizando docker-compose. Nosso ambiente possui dois containers, que são esses:

- `postgresql`: responsável pela execução do banco de dados
- `website`: responsável pela execução da aplicação web que roda em Ruby on Rails

A aplicação web pode ser acessada através do seu localhost plea port `3000`.

### Ngrok

O Ngrok é um programa de linha de comando que permite criar um túnel de conexão segura a partir do seu localhost e publicá-lo na internet.

Após a instalação execute o ngrok apontando para a porta 3000

    ./ngrok http 3000

## Variáveis de ambiente

A aplicação utilizará os arquivos `.env.website` e `.env.postgresql` para carregar as variáveis de ambiente necessárias para a execução. Existem dois arquivos(`.env.website.sample` e `.env.postgresql.sample`) com os exemplos de todas as variáveis necessários. Atenção com as seguintes variáveis:

- DEFAULT_USERNAME: Usuário padrão criado nos seeds

- DEFAULT_PASSWORD: Senha do usuário padrão criado nos seeds.

- GITHUB_WEBHOOK_SECRET: Secret utilizada na configuração do webhook no Github.

## Instalação

- Cire e defina as variáveis de ambiente nos arquivos `.env.website` e `.env.postgresql`.

* Faça o `build` da aplicação

Na primeira execução é necessário fazer o `build` para instalação completa com banco de dados e aplicação Ruby on Rails.

    $ docker-compose up --remove-orphans --build

Este comando irá criar todo ambiente básico da aplicação.

- Execute a migração de tabelas do banco de dados


        $ docker-compose exec website /bin/sh -c "DISABLE_SPRING=true bundle exec rake db:migrate"

- Execute os seeds da aplicação

Execute os seeds para a inserção de dados básicos de uso da aplicação

    $ docker-compose exec website /bin/sh -c "DISABLE_SPRING=true bundle exec rake db:seed"

Se todos os passos ocorreram com sucesso sua aplicação já pode ser utilizada pela porta `3000`.

## Executando a aplicação

Para apenas executar a aplicação com banco de dados execute o comando abaixo:

```
$ docker-compose up --remove-orphans
```

## Executar endpoints da aplicação

A aplicação possui dois endpoints:

- Recebe todos os eventos do do Github

- Endpoint: `POST http://username:password@localhost:3000/events`
- Atenção: para esse endpoint é necessário passar os dados de acesso para o basic auth que existe na aplicação.

- Lista todas ações de uma issue

  - Endpoint: `POST http://localhost:3000/issues/:numero_issue/actions`

## Testes

A aplicação possui testes que utilizam as seguintes ferramentas:

- rspec : Principal framework de testes
- database_cleaner: Para limpes do banco de dados durante a execução dos testes
- Faker: Para criar dados mais parecidos com o mundo real
- Factory Bot: Cria fábricas para facilitar a criação de objetos

### Execução

Para execução dos testes siga os passos abaixo:

- Prepare o ambiente de testes:

        docker-compose exec website /bin/sh -c "RAILS_ENV=test bundle exec rake db:test:prepare"

- Executar todos os testes:

        docker-compose exec website /bin/sh -c "RAILS_ENV=test bundle exec rspec"

## Configure os webhooks

No seu repositório do Github configure o webhook passando os dados configurados na aplicação

Segue os dados de exemplo abaixo:

- Payload URL: http://username:password@415b59a011c1.ngrok.io/events

  - username e password definidos nas variáveis `DEFAULT_USERNAME` e `DEFAULT_PASSWORD` respectivamente
  - Troque a url do ngrok para a retornada no retorno do comando durante a execução do mesmo.

- Content type: `application/x-www-form-urlencoded`

- Secret: Use uma string aleatória e segura
  - Deve ser a mesma definida na variável `GITHUB_WEBHOOK_SECRET`

## Tecnologias

### Aplicação

- Ruby '2.6.1'
- Rails '6.0.3'

### Bancos de dados

- PostgreSQL: 11-alpine
