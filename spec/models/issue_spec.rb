require 'rails_helper'

RSpec.describe Issue, type: :model do
  let(:subject) { build :issue }
  let(:content) { JSON.parse("{\"issue\":{\"url\":\"https://api.github.com/repos/hebertdougl/teste/issues/2\",\"number\":2,\"title\":\"Titulo da issue\"}}") }


  describe 'Associations' do
    it 'has many events' do
      should have_many(:events)
    end
  end

  describe 'Validations' do
    context 'is valid' do
      it 'with valid attributes' do
        expect(subject).to be_valid
      end
    end

    context 'is not valid' do
      it 'without a number' do
        subject.number = nil

        expect(subject).to_not be_valid
      end

      it 'without a title' do
        subject.title = nil

        expect(subject).to_not be_valid
      end

      it 'without a url' do
        subject.url = nil

        expect(subject).to_not be_valid
      end
    end
  end

  describe 'Attributes from content' do
    it 'should assign issue values' do
      issue = Issue.new
      issue.set_attributes_from_content(content)

      expect(issue.title).to eq('Titulo da issue')
      expect(issue.number).to eq(2)
      expect(issue.url).to eq('https://api.github.com/repos/hebertdougl/teste/issues/2')
    end
  end

  describe 'Create issue from content' do
    it 'should create if not exists' do
      issue = Issue.create_from_event(content)

      expect(issue.persisted?).to be true
    end

    it 'should return the same issue if it already exists' do
      issue = Issue.create_from_event(content)
      same_issue = Issue.create_from_event(content)

      expect(issue).to eq(same_issue)
    end
  end
end
