require 'rails_helper'

RSpec.describe Event, type: :model do
  let(:subject) { build :event }

  describe 'Associations' do
    it 'belongs to issue' do
      should belong_to(:issue)
    end
  end

  describe 'Validations' do
    context 'is valid' do
      it 'with valid attributes' do
        expect(subject).to be_valid
      end
    end

    context 'is not valid' do
      it 'without a name' do
        subject.name = nil

        expect(subject).to_not be_valid
      end

      it 'without a action' do
        subject.action = nil

        expect(subject).to_not be_valid
      end

      it 'without a content' do
        subject.content = nil

        expect(subject).to_not be_valid
      end
    end
  end

  describe 'create a issue' do
    it 'should create a issue if content is issue' do
      event = create :event

      expect(event.issue).to_not be nil
      expect(event.issue).to be_a Issue
      expect(event.issue_id).to eq(event.issue.id)
    end

    it 'should NOT create a issue if name not is a issue' do
      event = create :event, name: 'pull_request'

      expect(event.issue).to be nil
    end
  end
end
