require 'rails_helper'

RSpec.describe User, type: :model do
  let(:subject) { build :user }

  describe 'Validations' do
    context 'is valid' do
      it 'with valid attributes' do
        expect(subject).to be_valid
      end
    end

    context 'is not valid' do
      it 'without a username' do
        subject.username = nil

        expect(subject).to_not be_valid
      end

      it 'without a password' do
        subject.password = nil

        expect(subject).to_not be_valid
      end

      it 'if the username already exists' do
        subject.save
        new_object = build :user, username: subject.username

        expect(new_object).to_not be_valid
      end
    end
  end

  describe 'Encrypt password' do
    it 'should de encrypt password before validation' do
      new_object = build :user, username: subject.username, password: 'qwe123'
      new_object.valid?

      expect(new_object.encrypted_password).to_not be_empty
    end
  end

  describe 'Check username and password' do
    it 'should be return TRUE if username and password exists' do
      username = 'username_exists'
      password = 'password_exists'
      create :user, username: username, password: password

      result = User.check_password(username, password)

      expect(result).to be true
    end

    it 'should be return false if username not exists' do
      username = 'not_exists'
      password = 'any_password'

      result = User.check_password(username, password)

      expect(result).to be false
    end

    it 'should be return FALSE if password not exists' do
      username = 'username_exists'
      password = 'password_exists'
      create :user, username: username, password: password

      incorrect_password = 'incorrect_password'
      result = User.check_password(username, incorrect_password)

      expect(result).to be false
    end
  end
end
