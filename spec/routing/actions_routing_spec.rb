require "rails_helper"

RSpec.describe ActionsController, type: :routing do
  describe "routing" do
    it "routes to #show" do
      expect(get: "/issues/1/actions").to route_to("actions#show", issue_id: "1")
    end
  end
end
