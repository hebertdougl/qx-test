module AuthHelper
  def http_login(username, password)
    ActionController::HttpAuthentication::Basic.encode_credentials(username,password)
  end
end