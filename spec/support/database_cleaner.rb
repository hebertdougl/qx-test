# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:suite) do
    Faker::Config.locale = 'pt-BR'
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = :transaction
  end

  config.before(:each, js: true) do
    DatabaseCleaner.strategy = :truncation
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each, db: true) do
    DatabaseCleaner.clean
  end
end
