# frozen_string_literal: true

FactoryBot.define do
  factory :issue, class: Issue do
    sequence(:number)
    title { Faker::Lorem.question }
    url { Faker::Internet.url }
  end
end
