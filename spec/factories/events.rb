FactoryBot.define do
  factory :event, class: Event do
    name { 'issues' }
    action { 'edited' }
    content { JSON.parse("{\"issue\":{\"url\":\"https://api.github.com/repos/hebertdougl/teste/issues/2\",\"number\":2,\"title\":\"Titulo da issue\"}}") }
  end
end
