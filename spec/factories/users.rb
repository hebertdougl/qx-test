# frozen_string_literal: true

FactoryBot.define do
  factory :user, class: User do
    sequence(:username) { |n| "username-#{n.to_s.rjust(3, '0')}@teste.com" }
    password { 'qwe123' }
  end
end
