require 'rails_helper'

RSpec.describe "/events", type: :request do
  include AuthHelper

  let(:valid_attributes) {
    '{
      "action": "edited",
      "issue": {
        "url": "https://api.github.com/repos/hebertdougl/gobarber/issues/2",
        "repository_url": "https://api.github.com/repos/hebertdougl/gobarber",
        "labels_url": "https://api.github.com/repos/hebertdougl/gobarber/issues/2/labels{/name}",
        "comments_url": "https://api.github.com/repos/hebertdougl/gobarber/issues/2/comments",
        "events_url": "https://api.github.com/repos/hebertdougl/gobarber/issues/2/events",
        "html_url": "https://github.com/hebertdougl/gobarber/issues/2",
        "id": 632546290,
        "node_id": "MDU6SXNzdWU2MzI1NDYyOTA=",
        "number": 2,
        "title": "ss Troquei 2",
        "user": {
          "login": "hebertdougl",
          "id": 2739772,
          "node_id": "MDQ6VXNlcjI3Mzk3NzI=",
          "avatar_url": "https://avatars3.githubusercontent.com/u/2739772?v=4",
          "gravatar_id": "",
          "url": "https://api.github.com/users/hebertdougl",
          "html_url": "https://github.com/hebertdougl",
          "followers_url": "https://api.github.com/users/hebertdougl/followers",
          "following_url": "https://api.github.com/users/hebertdougl/following{/other_user}",
          "gists_url": "https://api.github.com/users/hebertdougl/gists{/gist_id}",
          "starred_url": "https://api.github.com/users/hebertdougl/starred{/owner}{/repo}",
          "subscriptions_url": "https://api.github.com/users/hebertdougl/subscriptions",
          "organizations_url": "https://api.github.com/users/hebertdougl/orgs",
          "repos_url": "https://api.github.com/users/hebertdougl/repos",
          "events_url": "https://api.github.com/users/hebertdougl/events{/privacy}",
          "received_events_url": "https://api.github.com/users/hebertdougl/received_events",
          "type": "User",
          "site_admin": false
        },
        "labels": [
        ],
        "state": "open",
        "locked": false,
        "assignee": null,
        "assignees": [
        ],
        "milestone": null,
        "comments": 0,
        "created_at": "2020-06-06T15:49:47Z",
        "updated_at": "2020-06-07T03:09:05Z",
        "closed_at": null,
        "author_association": "OWNER",
        "body": "Conteúdo"
      },
      "changes": {
        "title": {
          "from": "Troquei 2"
        }
      },
      "repository": {
        "id": 194011520,
        "node_id": "MDEwOlJlcG9zaXRvcnkxOTQwMTE1MjA=",
        "default_branch": "master"
      },
      "sender": {
        "login": "hebertdougl"
      }
    }'
  }

  let(:unauthenticated_headers) {
    {
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'GitHub-Hookshot/f1dd063',
      'X-GitHub-Delivery': '3eb1de80-a86c-11ea-8f60-2f84e13515dc',
      'X-GitHub-Event': 'issues',
      'X-Hub-Signature': 'sha1=4950ce4bd4defc898edc9472070bf3789765bfdb'
    }

  }
  let(:username) { 'username' }
  let(:password) { 'password' }
  let(:valid_headers) {
    {
      'HTTP_AUTHORIZATION': http_login(username, password),
      'Content-Type': 'application/x-www-form-urlencoded',
      'User-Agent': 'GitHub-Hookshot/f1dd063',
      'X-GitHub-Delivery': '3eb1de80-a86c-11ea-8f60-2f84e13515dc',
      'X-GitHub-Event': 'issues',
      'X-Hub-Signature': 'sha1=4950ce4bd4defc898edc9472070bf3789765bfdb'
    }
  }

  describe "POST /create" do
    context 'Unauthenticated' do
      it 'should return 401' do
        post events_url, params: { payload: valid_attributes }, headers: unauthenticated_headers

        expect(response).to have_http_status(:unauthorized)
      end
    end

    context "Authenticated" do
      before(:each) do
        # Create user to use for basic auth
        create :user, username: username, password: password
      end

      it "creates a new Event with valid parameters" do
        expect {
          post events_url, params: { payload: valid_attributes }, headers: valid_headers
        }.to change(Event, :count).by(1)
      end

      it "set a event name from headers" do
        post events_url, params: { payload: valid_attributes }, headers: valid_headers

        expect(assigns(:event_name)).to_not be_nil
        expect(assigns(:event_name)).to eq('issues')
      end
    end

    context 'With invalid signature' do
      it 'should return 401' do
        invalid_signature_headers = valid_headers.merge('X-Hub-Signature': 'sha1=bacbc511c1d3165_INVALID_bb0d92380d9513e9ca450c9d6')
        post events_url, params: { payload: valid_attributes }, headers: invalid_signature_headers

        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
