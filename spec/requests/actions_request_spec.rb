require 'rails_helper'

RSpec.describe "issues/:issue_id/actions", type: :request do
  describe "GET /show actions" do
    let(:event) { create :event }

    it "renders a successful response" do
      get action_url(event.issue.number), as: :json

      expect(response).to be_successful
      expect(response.content_type).to match(a_string_including("application/json"))
    end

    it "set a issue of event" do
      get action_url(event.issue.number), as: :json

      expect(assigns(:issue)).to_not be_nil
      expect(assigns(:issue)).to be_a(Issue)
      expect(assigns(:issue)).to eq(event.issue)
    end

    it "set a array of actions" do
      get action_url(event.issue.number), as: :json

      expect(assigns(:actions)).to_not be_nil
      expect(assigns(:actions)).to be_a(Array)
      expect(assigns(:actions)).to eq(event.issue.actions)
    end

    it "render not found if issue not exists" do
      invalid_number = 987654321

      expect{
        get action_url(invalid_number), as: :json
      }.to raise_error(ActionController::RoutingError)
    end
  end
end
