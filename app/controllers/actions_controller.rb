# frozen_string_literal: true

class ActionsController < ApplicationController
  before_action :set_issue, only: [:show]
  before_action :set_actions, only: [:show]

  def show
    render json: @actions
  end

  private

  def set_issue
    @issue = Issue.find_by(number: params[:issue_id])
    raise_not_found unless @issue
  end

  def set_actions
    @actions = @issue.actions
  end

  def raise_not_found
    raise ActionController::RoutingError, 'Not Found'
  end
end
