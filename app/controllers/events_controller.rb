# frozen_string_literal: true

class EventsController < ApplicationController
  before_action :set_event_name, only: [:create]
  before_action :http_basic_authenticate
  before_action :verify_signature

  # POST /events
  def create
    @event = Event.new(name: @event_name, action: action, content: event_params)

    if @event.save
      render json: @event, status: :created
    else
      render json: @event.errors, status: :unprocessable_entity
    end
  end

  private

  def http_basic_authenticate
    authenticate_or_request_with_http_basic do |username, password|
      User.check_password(username, password)
    end
  end

  def set_event_name
    @event_name = request.headers['X-GitHub-Event']
  end

  def action
    event_params['action']
  end

  def event_params
    JSON.parse(params.fetch(:payload))
  end

  def verify_signature
    signature = 'sha1=' + OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('sha1'), ENV['GITHUB_WEBHOOK_SECRET'], payload_body)
    equals_signature = Rack::Utils.secure_compare(signature, x_hub_signature)

    render_401 unless equals_signature
  end

  def x_hub_signature
    request.headers['X-Hub-Signature']
  end

  def payload_body
    request.body.rewind
    request.body.read
  end
end
