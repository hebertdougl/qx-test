# frozen_string_literal: true

require 'bcrypt'

class User < ApplicationRecord
  include BCrypt
  attr_accessor :password

  validates :username,           presence: true, uniqueness: true
  validates :encrypted_password, presence: true

  before_validation :encrypt_password, on: :create

  def self.check_password(username, password)
    user = User.find_by(username: username)
    return false unless user

    BCrypt::Password.new(user.encrypted_password) == password
  end

  private

  def encrypt_password
    enc_password = password.present? ? BCrypt::Password.create(password) : nil
    self.encrypted_password = enc_password
  end
end
