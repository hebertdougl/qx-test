# frozen_string_literal: true

class Event < ApplicationRecord
  validates :name,    presence: true
  validates :action,  presence: true
  validates :content, presence: true

  after_create :create_issue, if: :issue?

  belongs_to :issue, optional: true

  def create_issue
    issue = Issue.create_from_event(content)

    update(issue_id: issue.id)
  end

  private

  def issue?
    name == 'issues'
  end
end
