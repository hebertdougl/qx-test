# frozen_string_literal: true

class Issue < ApplicationRecord
  validates :number, presence: true
  validates :title,  presence: true
  validates :url,    presence: true

  has_many :events

  def self.create_from_event(content)
    issue = Issue.find_or_initialize_by(number: content['issue']['number'])
    return issue if issue.persisted?

    issue.set_attributes_from_content(content)
    issue.save!

    issue
  end

  def set_attributes_from_content(content)
    self.number = content['issue']['number']
    self.title  = content['issue']['title']
    self.url = content['issue']['url']
  end

  def actions
    events.map do |event|
      {
        action: event.action,
        created_at: event.created_at,
        updated_at: event.updated_at
      }
    end
  end
end
